---
layout: post
title:  "Issue 13"
date:   2018-09-03 06:20:20 -0700
categories: awsmobile weekly
---

AWS released a major update to the AWS Amplify toolchain and website which included support for native applications.  Read about it in [the announcement](https://aws.amazon.com/blogs/mobile/announcing-the-aws-amplify-cli-toolchain/), or check out [the website to get started](https://aws-amplify.github.io).

# Meetups

* [Building Serverless Web Applications with React & AWS Amplify with Nader Dabit](https://www.meetup.com/React-Chicago/events/250374886) in Chicago on September 26, via [Jon Gear](https://twitter.com/geareduptech)
* [Adbooker: The road to serverless GraphQL with AWS AppSync](https://www.meetup.com/AWS-User-Group-Norway/events/251958104/) in Oslo on October 16, via [Anders Bjoerne](https://twitter.com/abjoerne)

# AWS Services

* Blog: [Create a Multiuser GraphQL CRUD(L) App in 10 minutes with the new AWS Amplify CLI](https://medium.com/open-graphql/create-a-multiuser-graphql-crud-l-app-in-10-minutes-with-the-new-aws-amplify-cli-and-in-a-few-73aef3d49545) via [Ed Lima](https://twitter.com/ednergizer)
* Blog: [AWS AppSync With Lambda Data Sources](https://dzone.com/articles/aws-appsync-with-lambda-data-sources?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed:%20dzone%2Fcloud) via [Mat Warger](https://twitter.com/mwarger)
* Blog: [Build a GraphQL Service the easy way with AWS Amplify Model Transforms](https://medium.com/open-graphql/build-a-graphql-service-the-easy-way-with-aws-amplify-model-transforms-b3929b4f24c3) via [Adrian Hall](https://twitter.com/FizzyInTheHall)
* Blog: [AWS AppSync — the unexpected](https://medium.com/@dadc/aws-appsync-the-unexpected-a430ff7180a3) via [David C.](https://twitter.com/dadc)

# Native Development

* Blog: [Native Development with AWS Amplify](https://medium.com/@FizzyInTheHall/native-development-with-aws-amplify-f9bec8fdafe2) via [Adrian Hall](https://twitter.com/FizzyInTheHall)

# Web Development

* Blog: [Introducing the New AWS Amplify CLI Toolchain](https://hackernoon.com/introducing-the-new-aws-amplify-cli-toolchain-238157905c00) via [Nader Dabit](https://twitter.com/dabit3)
* Blog: [The VIA stack - Vue, Ionic & AppSync](https://medium.com/@bradpillow/the-via-stack-vue-ionic-appsync-bfc251ee6de6) via [Brad Pillow](https://twitter.com/BradPillow)

Have an update, content, conference session, meetup or anything else to share with the community?  [Send it to me](mailto:photoadrian@outlook.com) for inclusion in this digest of the AWS Mobile happenings.
