---
layout: post
title:  "Issue 19"
date:   2018-10-15 06:20:20 -0700
categories: awsmobile weekly
---

The [re:Invent session catalog](https://www.portal.reinvent.awsevents.com/connect/search.ww?trk=MOB#loadSearch-searchPhrase=&searchType=session&tc=0&sortBy=abbreviationSort&p=&i(10042)=24814) is live!  Check out all the great AWS Mobile content that will be available across more than 60 sessions and 5 workshops.

AWS Mobile will be live-streaming Mobile Week in San Francisco on Twitch TV.  If you have never been to an AWS Mobile loft week, this is your chance to tune in and see what happens over the course of a 3-day event.

# Meetups Webinars & Conferences
* [AWS Mobile Week](https://awsmobileweeksfoct2018.splashthat.com/) in San Francisco on October 16th-18th, via [Dennis Hills](https://twitter.com/dmennis) - also live streamed on [Twitch TV](https://twitch.tv/aws)!
* [Adbooker: The road to serverless GraphQL with AWS AppSync](https://www.meetup.com/AWS-User-Group-Norway/events/251958104/) in Oslo on October 16, via [Anders Bjoerne](https://twitter.com/abjoerne)
* [Chicago Serverless Miniconf 2018](https://www.eventbrite.com/e/serverless-mini-conf-with-aws-tickets-50745716805) in Chicago on November 3rd, via [Jon Gear](https://twitter.com/geareduptech)
* [AWS re:Invent 2018](https://reinvent.awsevents.com/) in Las Vegas on November 26th-30th, via [AWS re:Invent](https://twitter.com/AWSreInvent)

# AWS Services

* Slides from [Developing and Implementing APIs at Scale, the Serverless Way](https://www.slideshare.net/AmazonWebServices/aws-webinar-series-developing-and-implementing-apis-at-scale) from his recent webinar, via [Ed Lima](https://twitter.com/ednergizer)
* [appsync-cloudformation-transformer-cli](https://github.com/trek10inc/appsync-cloudformation-transformer-cli) - a mechanism for using the AWS AppSync Transformer Library with CloudFormation instead of the AWS Amplify library, via [Trek10](https://twitter.com/trek10inc)

# Native Development

* Blog: [Authentication with AWS Amplify: Google Login](https://itnext.io/authentication-with-aws-amplify-google-login-878aaf11f2d4) via [Adrian Hall](https://twitter.com/FizzyInTheHall)
* Blog: [Facebook Login using AWS Amplify and Amazon Cognito (iOS)](https://itnext.io/facebook-login-using-aws-amplify-and-amazon-cognito-4acf74875a04) via [Dennis Hills](https://twitter.com/dmennis)

# Web Development

* Blog: [Using Relay with AWS AppSync](https://medium.com/open-graphql/using-relay-with-aws-appsync-55c89ca02066) via [Kevin Bell](https://twitter.com/iamkevinbell)
* Sample: [AppSync Games Demo](https://github.com/mwarger/appsync-games-demo) from the conference presentation "Rethinking REST Practices with GraphQL" by [Matt Warger](https://twitter.com/mwarger)

Have an update, content, conference session, meetup or anything else to share with the community?  [Send it to me](mailto:photoadrian@outlook.com) for inclusion in this digest of the AWS Mobile happenings.
