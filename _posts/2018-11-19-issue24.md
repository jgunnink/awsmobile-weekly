---
layout: post
title:  "Issue 24"
date:   2018-11-19 06:20:20 -0700
categories: awsmobile weekly
---

The [re:Invent session catalog](https://www.portal.reinvent.awsevents.com/connect/search.ww?trk=MOB#loadSearch-searchPhrase=&searchType=session&tc=0&sortBy=abbreviationSort&p=&i(10042)=24814) is live!  Check out all the great AWS Mobile content that will be available across more than 60 sessions and 5 workshops. Also, several repeat and evening sessions this year.  For a mobile take on this, see [Your guide to AWS mobile and web development at re:Invent 2018](https://aws.amazon.com/blogs/mobile/your-guide-to-aws-mobile-and-web-development-at-reinvent-2018/)

Because of AWS re:Invent 2018, I will not be publishing next week.  Check out the live streams on [Twitch TV](https://twitch.tv/aws) and I will publish a wrap up of everything the following week.

# Meetups Webinars & Conferences

* [Modernising your REST APIs with GraphQL and AWS AppSync](https://aws.amazon.com/events/lunch-and-learn/dev-lounge/) in Auckland on November 20th, via [Muhammad Ali](https://twitter.com/mohdaliiqbal)
* [Modernising your REST APIs with GraphQL and AWS AppSync](https://aws.amazon.com/events/lunch-and-learn/dev-lounge/) in Melbourne on November 26th, via [Muhammad Ali](https://twitter.com/mohdaliiqbal)
* [AWS re:Invent 2018](https://reinvent.awsevents.com/) in Las Vegas on November 26th-30th, via [AWS re:Invent](https://twitter.com/AWSreInvent)
* [Building Serverless React Applications with AWS Amplify](https://frontend-con.io/product/building-serverless-react-applications-with-aws-amplify/) with Nader Dabit in Warsaw, Poland on December 3rd, via [Frontend Connect](https://twitter.com/FrontEndConnect)

# AWS Services

* Blog: [Building a GraphQL API by Example: Restaurant Reviews](https://medium.com/open-graphql/building-a-graphql-api-by-example-restaurant-reviews-acd80d60ec77) via [Adrian Hall](https://twitter.com/FizzyInTheHall)

# Native Development

* Blog: [AWS Amplify simplifies development for iOS and Android developers](https://aws.amazon.com/blogs/mobile/aws-amplify-simplifies-development-for-ios-and-android-developers/) via [Richard Threlkeld](https://twitter.com/undef_obj)
* Blog: [Federated Identities Using [Login with Amazon] with Amazon Cognito and AWS Amplify](https://itnext.io/federated-identities-using-login-with-amazon-with-amazon-cognito-and-aws-amplify-bfb7dfb7e185) via [Dennis Hills](https://twitter.com/dmennis)
* Blog: [Building an Android app with AWS Amplify – Part 1](https://aws.amazon.com/blogs/mobile/building-an-android-app-with-aws-amplify-part-1/) via [Jane Shen](https://twitter.com/j_shen)

# Web Development

* Blog: [Ionic 4 + AppSync: Use GraphQL mutations in Ionic (part 4)](https://gonehybrid.com/ionic-4-appsync-use-graphql-mutations-in-ionic-part-4/) via [Ashteya Biharisingh](https://twitter.com/ashteya)
* Sample: [Vue.js Chat App](https://github.com/r3-yamauchi/aws-amplify-vue-chat) via [@tetsushi_ito_](https://twitter.com/tetsushi_ito_)

Have an update, content, conference session, meetup or anything else to share with the community?  [Send it to me](mailto:photoadrian@outlook.com) for inclusion in this digest of the AWS Mobile happenings.
