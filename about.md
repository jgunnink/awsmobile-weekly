---
layout: page
title: About
permalink: /about/
---

I collect as many community updates around the AWS Mobile ecosystem as I can, releasing them as a blog post once a week.  Feel free to [follow me on Twitter](https://twitter.com/FizzyInTheHall) to receive notifications of new blog posts and community updates.

The AWS Mobile ecosystem covers the following AWS services:

* [AWS Mobile Hub](https://aws.amazon.com/mobile)
* [AWS AppSync](https://aws.amazon.com/appsync)
* [AWS Device Farm](https://aws.amazon.com/devicefarm)

In addition, I include relevant information about other services (like DynamoDB, Cognito, Pinpoint, API Gateway and Lambda) if the content is relevant to mobile developers.  I also include information about AWS SDKs, including [Amplify for JavaScript](https://aws.github.io/aws-amplify) and the AWS SDKs for Android and iOS.

If you wish to submit a community update, drop me [an email](mailto:phtooadrian@outlook.com).
