# AWS Mobile Weekly

This archive contains the source code for the AWS Mobile Weekly blog.  You can read the blog at https://adrianhall.gitlab.io/awsmobile-weekly.

## Contributing?

Contributions are welcomed.  The easiest way to contribute a link is to send it to me via email.  You can reach me at photoadrian@outlook.com.

Alternatively, the next issue is created in the `nextissue` branch as a new post.  Then, on Monday morning, I merge the branch into master and create the next issue.  The CI/CD pipeline on the master branch takes care of publication.  If you wish to contribute a new link via PR, then make the PR on the `nextissue` branch.
